import { useHistory } from "react-router-dom";
import "./style.css"

export default function Connected({ data }) {

    const history = useHistory()

    return (
        <div>
            <h1>Dados de Usuario</h1>
            <div className="dado">
                <div>Usuário:</div>
                <div>{data.name}</div>
            </div>
            <div className="dado">
                <div>Nome:</div>
                <div>{data.fullName}</div>
            </div>
            <div className="dado">
                <div>Email:</div>
                <div>{data.email}</div>
            </div>
            <div className="dado">
                <div>Senha:</div>
                <div>{data.password}</div>
            </div>
            <button onClick={() => history.push("/")}>VOLTAR</button>
        </div>
    )
}