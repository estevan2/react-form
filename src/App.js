import './App.css';
import { Switch, Route } from 'react-router-dom';
import { useState } from 'react';
import Form from './component/Form';
import Connected from './pages/Connected';

function App() {

  const [data, setData] = useState({})

  return (
    <div className="App">
      <main className="App-header">
        <Switch>
          <Route exact path="/">
            <Form setData={setData} />
          </Route>
          <Route exact path="/connected">
            <Connected data={data} />
          </Route>
        </Switch>
      </main>
    </div>
  );
}

export default App;
