import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup"
import { useHistory } from "react-router-dom";
import { useState } from "react";
import "./style.css";

export default function Form({ setData }) {

    const history = useHistory()

    const [checked, setChecked] = useState(false)

    const formSchema = yup.object().shape({
        name: yup.string().required("Nome obrigatório"),
        fullName: yup.string().required("Nome completo obrigatório"),
        email: yup.string().required("Email obrigatório").email("Email inválido"),
        confirmEmail: yup.string().required("Confirme seu email").oneOf([yup.ref("email")], "Email não confere"),
        password: yup.string().required("Senha obrigatória"),
        confirmPassword: yup.string().required("Confirme a senha").oneOf([yup.ref("password")], "Senha não confere")
    })

    const { register, handleSubmit, formState: { errors }, getValues } = useForm({
        resolver: yupResolver(formSchema)
    })

    const onSubmitFunction = (data) => {
        if (checked) {
            setData(data)
            history.push("/connected")
        }
    }

    return (
        <form onSubmit={handleSubmit(onSubmitFunction)}>
            <input placeholder="Nome de usuário*" {...register("name")} />
            <p>{errors.name?.message}</p>
            <input placeholder="Nome completo*" {...register("fullName")} />
            <p>{errors.fullName?.message}</p>
            <input placeholder="Endereço de Email*" {...register("email")} />
            <p>{errors.email?.message}</p>
            <input placeholder="Confirmar seu Email*" {...register("confirmEmail")} />
            <p>{errors.confirmEmail?.message}</p>
            <div>
                <input placeholder="Senha" {...register("password")} />
                <p>{errors.password?.message}</p>
                <input placeholder="Confirmar sua senha" {...register("confirmPassword")} />
                <p>{errors.confirmPassword?.message}</p>
            </div>
            <div className="checkbox">
                <input type="checkbox" onChange={() => setChecked(!checked)} /><span>Eu aceito os termos de uso da aplicação</span>
                <p>{!checked && "Você não aceitou os termos de uso"}</p>
            </div>
            <button type="submit">CADASTRO</button>
        </form>
    )
}